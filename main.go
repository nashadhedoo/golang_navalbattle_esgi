package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"
)

var intSizerBoard = 10
var ntTotalBoats = 15
var sName = ""
var sAnyKey = ""
var intPlayer1Turns = 0
var intPlayer2Turns = 0
var bPlayerFirst = true
var intPlayer1ShipsL = 15
var intPlayer2ShipsL = 15
var arrPlayer1Board [10][10]int
var arrPlayer2Board [10][10]int
const emptyElement = " "
const shipElement = "X"
const shipWreckElement = "+"
const missedElement = "o"

type playerStruct struct {
	port int
}
var players []playerStruct



func ServerScan() playerStruct{
	var player playerStruct
	fmt.Println("Port : " )
	fmt.Scanf("%d", &player.port)
	return player
}


func allowNot(w http.ResponseWriter, req *http.Request) {
	response(http.StatusMethodNotAllowed, w, req.Method+" is not allowed.")
}


func printInstructions() {
	fmt.Println("               Bataille naval ESGI")
}

func initializeBoard() {
	i := 0
	j := 0
	intPlayer1ShipsL = 15
	intPlayer2ShipsL = 15
	intPlayer1Turns = 0
	intPlayer2Turns = 0
	for i = 0; i < intSizerBoard; i++ {
		for j = 0; j < intSizerBoard; j++ {
			arrPlayer1Board[i][j] = 0
			arrPlayer2Board[i][j] = 0
		}
	}
}
func inputYourName() {
	fmt.Println("")
	fmt.Print("Entrez pseudo: ")
	fmt.Scanln(&sName)
	fmt.Println("")
	fmt.Println("Bonjour,", sName, "!")
	fmt.Println("")
	fmt.Println("Placez vos bateaux")
	pressKey()
}

func BoatsPos() {
	IntBoatscntr := 0
	intX := 0
	intY := 0
	sPos := ""
	fmt.Println("")
	for {
	enter:
		fmt.Print("Bateau situé > ")
		fmt.Scan(&sPos)

		if len(sPos) != 2 {
			fmt.Println("Position invalide")
			goto enter
		}
		sPos = strings.ToUpper(sPos)
		intX = int(sPos[0] - 65)
		intY = int(sPos[1] - 48)

		if intX < 0 || intX > 9 {
			fmt.Println("Position invalide")
			goto enter
		}

		if arrPlayer1Board[intX][intY] == 1 {
			arrPlayer1Board[intX][intY] = 0
		} else {
			arrPlayer1Board[intX][intY] = 1
			IntBoatscntr = IntBoatscntr + 1
		}
		BoardPos()

		if IntBoatscntr == 15 {
			break
		}
	}
}

func BoardPos() {
	fmt.Println(sName)
	i := 0
	j := 0
	intPlayer1ShipCntr := 0
	intPlayer2BoatCounter := 0
	sChar := 'A'
	fmt.Println(" 0123456789")
	for i = 0; i < intSizerBoard; i++ {
		fmt.Print(string(sChar))
		sChar = sChar + 1
		for j = 0; j < intSizerBoard; j++ {
			switch arrPlayer1Board[i][j] {
			case 0:
				fmt.Print(emptyElement)
			case 1:
				fmt.Print(shipElement)
				intPlayer1ShipCntr = intPlayer1ShipCntr + 1
			case 2:
				fmt.Print(shipWreckElement)
			case 3:
				fmt.Print(missedElement)
			}
		}
		fmt.Println("")
	}
	fmt.Println("Bateau", intPlayer1ShipCntr, "/", ntTotalBoats)
	fmt.Println("")
	fmt.Println("Joueur 2")
	i = 0
	j = 0
	sChar = 'A'
	fmt.Println(" 0123456789")
	for i = 0; i < intSizerBoard; i++ {
		fmt.Print(string(sChar))
		sChar = sChar + 1
		for j = 0; j < intSizerBoard; j++ {
			switch arrPlayer2Board[i][j] {
			case 0:
				fmt.Print(emptyElement)
			case 1:
				intPlayer2BoatCounter = intPlayer2BoatCounter + 1
			case 2:
				fmt.Print(shipWreckElement)
			case 3:
				fmt.Print(missedElement)
			}
		}
		fmt.Println("")
	}
	fmt.Println("Bateau", intPlayer2BoatCounter, "/", ntTotalBoats)
}
func failReq(w http.ResponseWriter, message string) {
	response(http.StatusBadRequest, w, message)
}

func response(status int, w http.ResponseWriter, message string) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, message)
}

func firstPlay() {
	IntFirstPlay1 := 0
	intPlayer2First := 0
	fmt.Println("")
	fmt.Println("Commencement")
	IntFirstPlay1 = rand.Intn(100)
	intPlayer2First = rand.Intn(100)
	if IntFirstPlay1 >= intPlayer2First {
		bPlayerFirst = true
		fmt.Println("Vous jouez en premier. Les nombres aléatoires disent", IntFirstPlay1, "vs.", intPlayer2First)
		fmt.Scanln(&sAnyKey)
		pressKey()
	} else {
		bPlayerFirst = false
		fmt.Println("Joueur 2 joue en premier", intPlayer2First, "vs.", IntFirstPlay1)
		fmt.Scanln(&sAnyKey)
		pressKey()
	}
}

func generatePlayer2Fleet() {
	IntBoatscntr := 0
	intRandomBoatatX := 0
	intRandomBoatatY := 0
	for {
		intRandomBoatatX = rand.Intn(10)
		intRandomBoatatY = rand.Intn(10)
		if arrPlayer2Board[intRandomBoatatX][intRandomBoatatY] == 0 {
			arrPlayer2Board[intRandomBoatatX][intRandomBoatatY] = 1
			IntBoatscntr = IntBoatscntr + 1
		}
		if IntBoatscntr == 15 {
			break
		}
	}
}

func moveOn() {
	intX := 0
	intY := 0
	sPos := ""
	fmt.Println("")
enter:
	for {
		fmt.Print("Envoyer missile teleguidé de la mort vers -->")
		fmt.Scan(&sPos)
		if len(sPos) != 2 {
			fmt.Println("Mauvaise position erreur")
			goto enter
		}
		sPos = strings.ToUpper(sPos)
		intX = int(sPos[0] - 65)
		intY = int(sPos[1] - 48)
		if intX < 0 || intX > 9 {
			fmt.Println("Mauvais position erreur")
			goto enter
		}
		intPlayer1Turns = intPlayer1Turns + 1
		if arrPlayer2Board[intX][intY] == 0 {
			fmt.Print("Tir manqué", sPos, ".")
			fmt.Scanln(&sAnyKey)
			pressKey()
		} else {
			arrPlayer2Board[intX][intY] = 2
			fmt.Print("Player 1 à touché", sPos, ".")
			fmt.Scanln(&sAnyKey)
			pressKey()
			BoardPos()
			intPlayer2ShipsL = intPlayer2ShipsL - 1
			if intPlayer2ShipsL == 0 {
				fmt.Println("Joueur 1 à gagné")
				fmt.Println(intPlayer1Turns, "Tour terminé")
				os.Exit(0)
			}
			goto enter
		}
		BoardPos()
		break
	}
}
func player2MoveOn() {
	intRandomBoatatX := 0
	intRandomBoatatY := 0
enter:
	for {
		intRandomBoatatX = rand.Intn(10)
		intRandomBoatatY = rand.Intn(10)
		intPlayer2Turns = intPlayer2Turns + 1
		if arrPlayer1Board[intRandomBoatatX][intRandomBoatatY] == 0 {
			arrPlayer1Board[intRandomBoatatX][intRandomBoatatY] = 3
			fmt.Println("Tir manqué", string(intRandomBoatatX+65), string(intRandomBoatatY+48))
			pressKey()
			BoardPos()
			break
		}
		if arrPlayer1Board[intRandomBoatatX][intRandomBoatatY] == 1 {
			arrPlayer1Board[intRandomBoatatX][intRandomBoatatY] = 2
			fmt.Println("Player 2 à touché", string(intRandomBoatatX+65), string(intRandomBoatatY+48))
			pressKey()
			BoardPos()
			intPlayer1ShipsL = intPlayer1ShipsL - 1
			if intPlayer1ShipsL == 0 {
				fmt.Println("Joueurs 2 à gagné")
				fmt.Println(intPlayer2Turns, "Tour terminé")
				os.Exit(0)
			} else {
				goto enter
			}
		}
		if arrPlayer1Board[intRandomBoatatX][intRandomBoatatY] == 2 {
			goto enter
		}
		if arrPlayer1Board[intRandomBoatatX][intRandomBoatatY] == 3 {
			goto enter
		}
	}
}

func portsAdd() []playerStruct{
	var players []playerStruct
	var player playerStruct
	var entry int
	fmt.Println(" Avec quel porte voulez vous jouer ?" )
	fmt.Scanf("%d", &player.port)
	players = append(players, player)

	fmt.Scanf("%d", &entry)

	for i := 0; i < entry ; i++ {
		fmt.Println("info joueur en cours ", i+1 )
		players = append(players,ServerScan())
	}

	return players
}
func pressKey() {
	in := bufio.NewReader(os.Stdin)
	line, _ := in.ReadString('\n')
	_ = line
}







func main() {
	rand.Seed(time.Now().UnixNano())
	players = portsAdd()
	printInstructions()
	initializeBoard()
	inputYourName()
	BoardPos()
	BoatsPos()
	generatePlayer2Fleet()
	BoardPos()
	firstPlay()
	for {
		if bPlayerFirst {
			moveOn()
			player2MoveOn()
		} else {
			player2MoveOn()
			moveOn()
		}
	}
}